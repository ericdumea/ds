package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import com.google.gson.Gson;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;
import ro.tuc.dsrl.ds.handson.assig.three.queue.model.Dvd;

import java.io.IOException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientStart {

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost",8888);

		MailService mailService = new MailService("DVD_SERVICE","your_password_here");
		String message;
		Gson gson = new Gson();

		while(true) {
			try {
				message = queue.readMessage();
				Dvd dvd = null;
				if(message.contains("transmit")) {

					System.out.println(message.split(";")[1]);
					dvd = gson.fromJson(message.split(";")[1], Dvd.class);
					System.out.println(dvd.getTitle());
				}

				mailService.sendMail("to@me.me","Dummy Mail Title",gson.toJson(dvd));
				System.out.println("Mail sent");

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
