package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileConsumer {

    public FileConsumer() {
    }

    public static void main(String[] args) {
        QueueServerConnection queue = new QueueServerConnection("localhost",8888);

        int i=0;

        String message;

        while(true) {
            try {
                message = queue.readMessage();

                BufferedWriter writer = new BufferedWriter(new FileWriter("file"+(i++)+".txt",true));
                writer.append(message);
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
