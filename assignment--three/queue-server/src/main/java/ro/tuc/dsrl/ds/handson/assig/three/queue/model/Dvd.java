package ro.tuc.dsrl.ds.handson.assig.three.queue.model;

public class Dvd {

    private String title;

    private Double price;

    private Integer year;

    public Dvd() {
    }

    public Dvd(String title, Double price, Integer year) {
        this.title = title;
        this.price = price;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
