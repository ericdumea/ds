package ro.utcn.ds.client;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ro.utcn.ds.common.entities.Car;
import ro.utcn.ds.common.serviceinterfaces.IPriceService;
import ro.utcn.ds.common.serviceinterfaces.ITaxService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class AppController {
    public TextField purchaseTF;
    public TextField engineTF;
    public Button btnCompute;
    public TextField priceTF;
    public Label labelResult;


    public void compute(ActionEvent actionEvent) throws RemoteException {

        Double purchasePrice = Double.parseDouble(priceTF.getText());

        Integer engineCapacity = Integer.parseInt(engineTF.getText());

        Integer yearOfPurchase = Integer.parseInt(purchaseTF.getText());

        Registry registry = LocateRegistry.getRegistry();
        ITaxService taxService = null;
        IPriceService priceService = null;
        try {
            taxService = (ITaxService) registry.lookup("ITaxService");
            priceService = (IPriceService) registry.lookup("IPriceService");
        } catch (NotBoundException e) {
            e.printStackTrace();
        }

        Car car = new Car(yearOfPurchase,engineCapacity,purchasePrice);

        StringBuilder result = new StringBuilder();
        result.append("Tax: ");
        result.append(taxService.computeTax(car));
        result.append("$   Price: ");
        result.append(priceService.computePrice(car));

        labelResult.setText(result.toString());

    }
}
