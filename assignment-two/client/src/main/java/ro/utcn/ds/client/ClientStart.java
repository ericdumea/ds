package ro.utcn.ds.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class ClientStart extends Application {

    public ClientStart() {
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root;

        try {
            root = FXMLLoader.load(getClass().getResource("/LoginView.fxml"));
        }
        catch (Exception ex){
            ex.printStackTrace();

            return;
        }
        primaryStage.setTitle("Ping-Pong Tournament");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();


    }

    public static void main(String[] args) throws IOException {
        launch(args);
    }
}
