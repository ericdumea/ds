package ro.utcn.ds.common.serviceinterfaces;

import ro.utcn.ds.common.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IPriceService extends Remote {

    Double computePrice(Car car) throws RemoteException;

}
