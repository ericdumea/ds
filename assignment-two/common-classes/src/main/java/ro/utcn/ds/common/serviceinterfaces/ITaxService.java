package ro.utcn.ds.common.serviceinterfaces;

import ro.utcn.ds.common.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ITaxService extends Remote {

	double computeTax(Car c) throws RemoteException;

}
