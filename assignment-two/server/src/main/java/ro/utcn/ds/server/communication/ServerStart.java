package ro.utcn.ds.server.communication;

import ro.utcn.ds.common.serviceinterfaces.IPriceService;
import ro.utcn.ds.common.serviceinterfaces.ITaxService;
import ro.utcn.ds.server.services.PriceService;
import ro.utcn.ds.server.services.TaxService;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class ServerStart {

    private static final int PORT = 1099;

    private ServerStart() {
    }

    public static void main(String[] args) {

        Registry registry;
        IPriceService iPriceService;
        ITaxService iTaxService;
        try {
            registry = LocateRegistry.createRegistry(PORT);
            iPriceService = new PriceService();
            iTaxService = new TaxService();

        } catch (RemoteException e) {
            e.printStackTrace();
            return;
        }


        try {
            registry.rebind("IPriceService", iPriceService);
            registry.rebind("ITaxService", iTaxService);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }
}
