package ro.utcn.ds.server.services;

import ro.utcn.ds.common.entities.Car;
import ro.utcn.ds.common.serviceinterfaces.IPriceService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class PriceService extends UnicastRemoteObject implements IPriceService {

    public PriceService() throws RemoteException {
    }

    @Override
    public Double computePrice(Car car) {
        Double sellPrice = 0.0;

        if ((2018 - car.getYear()) < 7) {
            sellPrice = car.getPurchasePrice() - (car.getPurchasePrice() / 7) * (2018 - car.getYear());
        }

        return sellPrice;
    }
}
